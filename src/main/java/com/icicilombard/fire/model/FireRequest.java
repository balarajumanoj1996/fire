//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2023.03.06 at 02:31:24 PM IST 
//


package com.icicilombard.fire.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for FireRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FireRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="policyStartDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="quoteCreationDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="policyType" type="{http://fire.icicilombard.com}PolicyType"/>
 *         &lt;element name="requiredRiskInspectionFlag" type="{http://fire.icicilombard.com}RiskInspectionType"/>
 *         &lt;element name="policyEndDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="taxEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="taxDetails" type="{http://fire.icicilombard.com}TaxDetails"/>
 *         &lt;element name="coinsuranceDetail" type="{http://fire.icicilombard.com}CoinsuranceDetail"/>
 *         &lt;element name="plinthAndFoundationRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="declarationProvision" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="floaterProvision" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="floaterLoadingOnTerrorism" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="floaterDeclarationProvision" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="shortPolicyRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="shortPolicyPeriod" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="shortPeriodType" type="{http://fire.icicilombard.com}ShortPeriodType"/>
 *         &lt;element name="proRataPeriod" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="longPolicyRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="longPolicyPeriod" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="locationDetails" type="{http://fire.icicilombard.com}LocationDetail" maxOccurs="unbounded"/>
 *         &lt;element name="sourceSystem" type="{http://fire.icicilombard.com}SourceSystem"/>
 *         &lt;element name="stampDutyDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="userRole" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="policyCover" type="{http://fire.icicilombard.com}PolicyCoverDetail" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="backdationDetails" type="{http://fire.icicilombard.com}BackdationDetails" minOccurs="0"/>
 *         &lt;element name="skipUWApproval" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="goalSeekOption" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="goalSeekOptions" type="{http://fire.icicilombard.com}GoalSeekOptions"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FireRequest", propOrder = {
    "requestID",
    "policyStartDate",
    "quoteCreationDate",
    "policyType",
    "requiredRiskInspectionFlag",
    "policyEndDate",
    "taxEffectiveDate",
    "taxDetails",
    "coinsuranceDetail",
    "plinthAndFoundationRequired",
    "declarationProvision",
    "floaterProvision",
    "floaterLoadingOnTerrorism",
    "floaterDeclarationProvision",
    "shortPolicyRequired",
    "shortPolicyPeriod",
    "shortPeriodType",
    "proRataPeriod",
    "longPolicyRequired",
    "longPolicyPeriod",
    "locationDetails",
    "sourceSystem",
    "stampDutyDate",
    "userRole",
    "policyCover",
    "backdationDetails",
    "skipUWApproval",
    "goalSeekOption",
    "goalSeekOptions"
})
public class FireRequest
    implements Serializable
{

    private final static long serialVersionUID = -6026937020915831338L;
    @XmlElement(required = true)
    protected String requestID;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "date")
    protected Date policyStartDate;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "date")
    protected Date quoteCreationDate;
    @XmlElement(required = true)
    protected PolicyType policyType;
    @XmlElement(required = true)
    protected RiskInspectionType requiredRiskInspectionFlag;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "date")
    protected Date policyEndDate;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "date")
    protected Date taxEffectiveDate;
    @XmlElement(required = true)
    protected TaxDetails taxDetails;
    @XmlElement(required = true)
    protected CoinsuranceDetail coinsuranceDetail;
    protected boolean plinthAndFoundationRequired;
    protected boolean declarationProvision;
    protected boolean floaterProvision;
    protected boolean floaterLoadingOnTerrorism;
    protected boolean floaterDeclarationProvision;
    protected boolean shortPolicyRequired;
    @XmlElement(required = true)
    protected BigDecimal shortPolicyPeriod;
    @XmlElement(required = true)
    protected ShortPeriodType shortPeriodType;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "date")
    protected Date proRataPeriod;
    protected boolean longPolicyRequired;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "integer")
    protected Integer longPolicyPeriod;
    @XmlElement(required = true)
    protected com.icicilombard.fire.model.LocationDetail[] locationDetails;
    @XmlElement(required = true)
    protected SourceSystem sourceSystem;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "date")
    protected Date stampDutyDate;
    @XmlElement(required = true)
    protected String userRole;
    protected com.icicilombard.fire.model.PolicyCoverDetail[] policyCover;
    protected BackdationDetails backdationDetails;
    protected boolean skipUWApproval;
    protected boolean goalSeekOption;
    @XmlElement(required = true)
    protected GoalSeekOptions goalSeekOptions;

    /**
     * Gets the value of the requestID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestID() {
        return requestID;
    }

    /**
     * Sets the value of the requestID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestID(String value) {
        this.requestID = value;
    }

    /**
     * Gets the value of the policyStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getPolicyStartDate() {
        return policyStartDate;
    }

    /**
     * Sets the value of the policyStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyStartDate(Date value) {
        this.policyStartDate = value;
    }

    /**
     * Gets the value of the quoteCreationDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getQuoteCreationDate() {
        return quoteCreationDate;
    }

    /**
     * Sets the value of the quoteCreationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuoteCreationDate(Date value) {
        this.quoteCreationDate = value;
    }

    /**
     * Gets the value of the policyType property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyType }
     *     
     */
    public PolicyType getPolicyType() {
        return policyType;
    }

    /**
     * Sets the value of the policyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyType }
     *     
     */
    public void setPolicyType(PolicyType value) {
        this.policyType = value;
    }

    /**
     * Gets the value of the requiredRiskInspectionFlag property.
     * 
     * @return
     *     possible object is
     *     {@link RiskInspectionType }
     *     
     */
    public RiskInspectionType getRequiredRiskInspectionFlag() {
        return requiredRiskInspectionFlag;
    }

    /**
     * Sets the value of the requiredRiskInspectionFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link RiskInspectionType }
     *     
     */
    public void setRequiredRiskInspectionFlag(RiskInspectionType value) {
        this.requiredRiskInspectionFlag = value;
    }

    /**
     * Gets the value of the policyEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getPolicyEndDate() {
        return policyEndDate;
    }

    /**
     * Sets the value of the policyEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyEndDate(Date value) {
        this.policyEndDate = value;
    }

    /**
     * Gets the value of the taxEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getTaxEffectiveDate() {
        return taxEffectiveDate;
    }

    /**
     * Sets the value of the taxEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxEffectiveDate(Date value) {
        this.taxEffectiveDate = value;
    }

    /**
     * Gets the value of the taxDetails property.
     * 
     * @return
     *     possible object is
     *     {@link TaxDetails }
     *     
     */
    public TaxDetails getTaxDetails() {
        return taxDetails;
    }

    /**
     * Sets the value of the taxDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxDetails }
     *     
     */
    public void setTaxDetails(TaxDetails value) {
        this.taxDetails = value;
    }

    /**
     * Gets the value of the coinsuranceDetail property.
     * 
     * @return
     *     possible object is
     *     {@link CoinsuranceDetail }
     *     
     */
    public CoinsuranceDetail getCoinsuranceDetail() {
        return coinsuranceDetail;
    }

    /**
     * Sets the value of the coinsuranceDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link CoinsuranceDetail }
     *     
     */
    public void setCoinsuranceDetail(CoinsuranceDetail value) {
        this.coinsuranceDetail = value;
    }

    /**
     * Gets the value of the plinthAndFoundationRequired property.
     * 
     */
    public boolean isPlinthAndFoundationRequired() {
        return plinthAndFoundationRequired;
    }

    /**
     * Sets the value of the plinthAndFoundationRequired property.
     * 
     */
    public void setPlinthAndFoundationRequired(boolean value) {
        this.plinthAndFoundationRequired = value;
    }

    /**
     * Gets the value of the declarationProvision property.
     * 
     */
    public boolean isDeclarationProvision() {
        return declarationProvision;
    }

    /**
     * Sets the value of the declarationProvision property.
     * 
     */
    public void setDeclarationProvision(boolean value) {
        this.declarationProvision = value;
    }

    /**
     * Gets the value of the floaterProvision property.
     * 
     */
    public boolean isFloaterProvision() {
        return floaterProvision;
    }

    /**
     * Sets the value of the floaterProvision property.
     * 
     */
    public void setFloaterProvision(boolean value) {
        this.floaterProvision = value;
    }

    /**
     * Gets the value of the floaterLoadingOnTerrorism property.
     * 
     */
    public boolean isFloaterLoadingOnTerrorism() {
        return floaterLoadingOnTerrorism;
    }

    /**
     * Sets the value of the floaterLoadingOnTerrorism property.
     * 
     */
    public void setFloaterLoadingOnTerrorism(boolean value) {
        this.floaterLoadingOnTerrorism = value;
    }

    /**
     * Gets the value of the floaterDeclarationProvision property.
     * 
     */
    public boolean isFloaterDeclarationProvision() {
        return floaterDeclarationProvision;
    }

    /**
     * Sets the value of the floaterDeclarationProvision property.
     * 
     */
    public void setFloaterDeclarationProvision(boolean value) {
        this.floaterDeclarationProvision = value;
    }

    /**
     * Gets the value of the shortPolicyRequired property.
     * 
     */
    public boolean isShortPolicyRequired() {
        return shortPolicyRequired;
    }

    /**
     * Sets the value of the shortPolicyRequired property.
     * 
     */
    public void setShortPolicyRequired(boolean value) {
        this.shortPolicyRequired = value;
    }

    /**
     * Gets the value of the shortPolicyPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getShortPolicyPeriod() {
        return shortPolicyPeriod;
    }

    /**
     * Sets the value of the shortPolicyPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setShortPolicyPeriod(BigDecimal value) {
        this.shortPolicyPeriod = value;
    }

    /**
     * Gets the value of the shortPeriodType property.
     * 
     * @return
     *     possible object is
     *     {@link ShortPeriodType }
     *     
     */
    public ShortPeriodType getShortPeriodType() {
        return shortPeriodType;
    }

    /**
     * Sets the value of the shortPeriodType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShortPeriodType }
     *     
     */
    public void setShortPeriodType(ShortPeriodType value) {
        this.shortPeriodType = value;
    }

    /**
     * Gets the value of the proRataPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getProRataPeriod() {
        return proRataPeriod;
    }

    /**
     * Sets the value of the proRataPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProRataPeriod(Date value) {
        this.proRataPeriod = value;
    }

    /**
     * Gets the value of the longPolicyRequired property.
     * 
     */
    public boolean isLongPolicyRequired() {
        return longPolicyRequired;
    }

    /**
     * Sets the value of the longPolicyRequired property.
     * 
     */
    public void setLongPolicyRequired(boolean value) {
        this.longPolicyRequired = value;
    }

    /**
     * Gets the value of the longPolicyPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Integer getLongPolicyPeriod() {
        return longPolicyPeriod;
    }

    /**
     * Sets the value of the longPolicyPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLongPolicyPeriod(Integer value) {
        this.longPolicyPeriod = value;
    }

    /**
     * 
     * 
     * @return
     *     array of
     *     {@link com.icicilombard.fire.model.LocationDetail }
     *     
     */
    public com.icicilombard.fire.model.LocationDetail[] getLocationDetails() {
        if (this.locationDetails == null) {
            return new com.icicilombard.fire.model.LocationDetail[ 0 ] ;
        }
        com.icicilombard.fire.model.LocationDetail[] retVal = new com.icicilombard.fire.model.LocationDetail[this.locationDetails.length] ;
        System.arraycopy(this.locationDetails, 0, retVal, 0, this.locationDetails.length);
        return (retVal);
    }

    /**
     * 
     * 
     * @return
     *     one of
     *     {@link com.icicilombard.fire.model.LocationDetail }
     *     
     */
    public com.icicilombard.fire.model.LocationDetail getLocationDetails(int idx) {
        if (this.locationDetails == null) {
            throw new IndexOutOfBoundsException();
        }
        return this.locationDetails[idx];
    }

    public int getLocationDetailsLength() {
        if (this.locationDetails == null) {
            return  0;
        }
        return this.locationDetails.length;
    }

    /**
     * 
     * 
     * @param values
     *     allowed objects are
     *     {@link com.icicilombard.fire.model.LocationDetail }
     *     
     */
    public void setLocationDetails(com.icicilombard.fire.model.LocationDetail[] values) {
        int len = values.length;
        this.locationDetails = ((com.icicilombard.fire.model.LocationDetail[]) new com.icicilombard.fire.model.LocationDetail[len] );
        for (int i = 0; (i<len); i ++) {
            this.locationDetails[i] = values[i];
        }
    }

    /**
     * 
     * 
     * @param value
     *     allowed object is
     *     {@link com.icicilombard.fire.model.LocationDetail }
     *     
     */
    public com.icicilombard.fire.model.LocationDetail setLocationDetails(int idx, com.icicilombard.fire.model.LocationDetail value) {
        return this.locationDetails[idx] = value;
    }

    /**
     * Gets the value of the sourceSystem property.
     * 
     * @return
     *     possible object is
     *     {@link SourceSystem }
     *     
     */
    public SourceSystem getSourceSystem() {
        return sourceSystem;
    }

    /**
     * Sets the value of the sourceSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link SourceSystem }
     *     
     */
    public void setSourceSystem(SourceSystem value) {
        this.sourceSystem = value;
    }

    /**
     * Gets the value of the stampDutyDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getStampDutyDate() {
        return stampDutyDate;
    }

    /**
     * Sets the value of the stampDutyDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStampDutyDate(Date value) {
        this.stampDutyDate = value;
    }

    /**
     * Gets the value of the userRole property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserRole() {
        return userRole;
    }

    /**
     * Sets the value of the userRole property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserRole(String value) {
        this.userRole = value;
    }

    /**
     * 
     * 
     * @return
     *     array of
     *     {@link com.icicilombard.fire.model.PolicyCoverDetail }
     *     
     */
    public com.icicilombard.fire.model.PolicyCoverDetail[] getPolicyCover() {
        if (this.policyCover == null) {
            return new com.icicilombard.fire.model.PolicyCoverDetail[ 0 ] ;
        }
        com.icicilombard.fire.model.PolicyCoverDetail[] retVal = new com.icicilombard.fire.model.PolicyCoverDetail[this.policyCover.length] ;
        System.arraycopy(this.policyCover, 0, retVal, 0, this.policyCover.length);
        return (retVal);
    }

    /**
     * 
     * 
     * @return
     *     one of
     *     {@link com.icicilombard.fire.model.PolicyCoverDetail }
     *     
     */
    public com.icicilombard.fire.model.PolicyCoverDetail getPolicyCover(int idx) {
        if (this.policyCover == null) {
            throw new IndexOutOfBoundsException();
        }
        return this.policyCover[idx];
    }

    public int getPolicyCoverLength() {
        if (this.policyCover == null) {
            return  0;
        }
        return this.policyCover.length;
    }

    /**
     * 
     * 
     * @param values
     *     allowed objects are
     *     {@link com.icicilombard.fire.model.PolicyCoverDetail }
     *     
     */
    public void setPolicyCover(com.icicilombard.fire.model.PolicyCoverDetail[] values) {
        int len = values.length;
        this.policyCover = ((com.icicilombard.fire.model.PolicyCoverDetail[]) new com.icicilombard.fire.model.PolicyCoverDetail[len] );
        for (int i = 0; (i<len); i ++) {
            this.policyCover[i] = values[i];
        }
    }

    /**
     * 
     * 
     * @param value
     *     allowed object is
     *     {@link com.icicilombard.fire.model.PolicyCoverDetail }
     *     
     */
    public com.icicilombard.fire.model.PolicyCoverDetail setPolicyCover(int idx, com.icicilombard.fire.model.PolicyCoverDetail value) {
        return this.policyCover[idx] = value;
    }

    /**
     * Gets the value of the backdationDetails property.
     * 
     * @return
     *     possible object is
     *     {@link BackdationDetails }
     *     
     */
    public BackdationDetails getBackdationDetails() {
        return backdationDetails;
    }

    /**
     * Sets the value of the backdationDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link BackdationDetails }
     *     
     */
    public void setBackdationDetails(BackdationDetails value) {
        this.backdationDetails = value;
    }

    /**
     * Gets the value of the skipUWApproval property.
     * 
     */
    public boolean isSkipUWApproval() {
        return skipUWApproval;
    }

    /**
     * Sets the value of the skipUWApproval property.
     * 
     */
    public void setSkipUWApproval(boolean value) {
        this.skipUWApproval = value;
    }

    /**
     * Gets the value of the goalSeekOption property.
     * 
     */
    public boolean isGoalSeekOption() {
        return goalSeekOption;
    }

    /**
     * Sets the value of the goalSeekOption property.
     * 
     */
    public void setGoalSeekOption(boolean value) {
        this.goalSeekOption = value;
    }

    /**
     * Gets the value of the goalSeekOptions property.
     * 
     * @return
     *     possible object is
     *     {@link GoalSeekOptions }
     *     
     */
    public GoalSeekOptions getGoalSeekOptions() {
        return goalSeekOptions;
    }

    /**
     * Sets the value of the goalSeekOptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link GoalSeekOptions }
     *     
     */
    public void setGoalSeekOptions(GoalSeekOptions value) {
        this.goalSeekOptions = value;
    }

}
