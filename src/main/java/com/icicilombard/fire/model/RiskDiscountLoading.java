//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2023.03.06 at 02:31:24 PM IST 
//


package com.icicilombard.fire.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RiskDiscountLoading complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RiskDiscountLoading">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="optionI" type="{http://fire.icicilombard.com}OptionI"/>
 *         &lt;element name="optionII" type="{http://fire.icicilombard.com}OptionII"/>
 *         &lt;element name="claimExperience" type="{http://fire.icicilombard.com}ClaimExperience" maxOccurs="2"/>
 *         &lt;element name="voluntaryDeductible" type="{http://fire.icicilombard.com}VoluntaryDeductible" maxOccurs="3"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RiskDiscountLoading", propOrder = {
    "optionI",
    "optionII",
    "claimExperience",
    "voluntaryDeductible"
})
public class RiskDiscountLoading
    implements Serializable
{

    private final static long serialVersionUID = -6026937020915831338L;
    @XmlElement(required = true)
    protected OptionI optionI;
    @XmlElement(required = true)
    protected OptionII optionII;
    @XmlElement(required = true)
    protected com.icicilombard.fire.model.ClaimExperience[] claimExperience;
    @XmlElement(required = true)
    protected com.icicilombard.fire.model.VoluntaryDeductible[] voluntaryDeductible;

    /**
     * Gets the value of the optionI property.
     * 
     * @return
     *     possible object is
     *     {@link OptionI }
     *     
     */
    public OptionI getOptionI() {
        return optionI;
    }

    /**
     * Sets the value of the optionI property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionI }
     *     
     */
    public void setOptionI(OptionI value) {
        this.optionI = value;
    }

    /**
     * Gets the value of the optionII property.
     * 
     * @return
     *     possible object is
     *     {@link OptionII }
     *     
     */
    public OptionII getOptionII() {
        return optionII;
    }

    /**
     * Sets the value of the optionII property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionII }
     *     
     */
    public void setOptionII(OptionII value) {
        this.optionII = value;
    }

    /**
     * 
     * 
     * @return
     *     array of
     *     {@link com.icicilombard.fire.model.ClaimExperience }
     *     
     */
    public com.icicilombard.fire.model.ClaimExperience[] getClaimExperience() {
        if (this.claimExperience == null) {
            return new com.icicilombard.fire.model.ClaimExperience[ 0 ] ;
        }
        com.icicilombard.fire.model.ClaimExperience[] retVal = new com.icicilombard.fire.model.ClaimExperience[this.claimExperience.length] ;
        System.arraycopy(this.claimExperience, 0, retVal, 0, this.claimExperience.length);
        return (retVal);
    }

    /**
     * 
     * 
     * @return
     *     one of
     *     {@link com.icicilombard.fire.model.ClaimExperience }
     *     
     */
    public com.icicilombard.fire.model.ClaimExperience getClaimExperience(int idx) {
        if (this.claimExperience == null) {
            throw new IndexOutOfBoundsException();
        }
        return this.claimExperience[idx];
    }

    public int getClaimExperienceLength() {
        if (this.claimExperience == null) {
            return  0;
        }
        return this.claimExperience.length;
    }

    /**
     * 
     * 
     * @param values
     *     allowed objects are
     *     {@link com.icicilombard.fire.model.ClaimExperience }
     *     
     */
    public void setClaimExperience(com.icicilombard.fire.model.ClaimExperience[] values) {
        int len = values.length;
        this.claimExperience = ((com.icicilombard.fire.model.ClaimExperience[]) new com.icicilombard.fire.model.ClaimExperience[len] );
        for (int i = 0; (i<len); i ++) {
            this.claimExperience[i] = values[i];
        }
    }

    /**
     * 
     * 
     * @param value
     *     allowed object is
     *     {@link com.icicilombard.fire.model.ClaimExperience }
     *     
     */
    public com.icicilombard.fire.model.ClaimExperience setClaimExperience(int idx, com.icicilombard.fire.model.ClaimExperience value) {
        return this.claimExperience[idx] = value;
    }

    /**
     * 
     * 
     * @return
     *     array of
     *     {@link com.icicilombard.fire.model.VoluntaryDeductible }
     *     
     */
    public com.icicilombard.fire.model.VoluntaryDeductible[] getVoluntaryDeductible() {
        if (this.voluntaryDeductible == null) {
            return new com.icicilombard.fire.model.VoluntaryDeductible[ 0 ] ;
        }
        com.icicilombard.fire.model.VoluntaryDeductible[] retVal = new com.icicilombard.fire.model.VoluntaryDeductible[this.voluntaryDeductible.length] ;
        System.arraycopy(this.voluntaryDeductible, 0, retVal, 0, this.voluntaryDeductible.length);
        return (retVal);
    }

    /**
     * 
     * 
     * @return
     *     one of
     *     {@link com.icicilombard.fire.model.VoluntaryDeductible }
     *     
     */
    public com.icicilombard.fire.model.VoluntaryDeductible getVoluntaryDeductible(int idx) {
        if (this.voluntaryDeductible == null) {
            throw new IndexOutOfBoundsException();
        }
        return this.voluntaryDeductible[idx];
    }

    public int getVoluntaryDeductibleLength() {
        if (this.voluntaryDeductible == null) {
            return  0;
        }
        return this.voluntaryDeductible.length;
    }

    /**
     * 
     * 
     * @param values
     *     allowed objects are
     *     {@link com.icicilombard.fire.model.VoluntaryDeductible }
     *     
     */
    public void setVoluntaryDeductible(com.icicilombard.fire.model.VoluntaryDeductible[] values) {
        int len = values.length;
        this.voluntaryDeductible = ((com.icicilombard.fire.model.VoluntaryDeductible[]) new com.icicilombard.fire.model.VoluntaryDeductible[len] );
        for (int i = 0; (i<len); i ++) {
            this.voluntaryDeductible[i] = values[i];
        }
    }

    /**
     * 
     * 
     * @param value
     *     allowed object is
     *     {@link com.icicilombard.fire.model.VoluntaryDeductible }
     *     
     */
    public com.icicilombard.fire.model.VoluntaryDeductible setVoluntaryDeductible(int idx, com.icicilombard.fire.model.VoluntaryDeductible value) {
        return this.voluntaryDeductible[idx] = value;
    }

}
