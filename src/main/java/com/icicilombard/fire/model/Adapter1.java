//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2023.03.06 at 02:31:24 PM IST 
//


package com.icicilombard.fire.model;

import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class Adapter1
    extends XmlAdapter<String, Date>
{


    public Date unmarshal(String value) {
        return (com.icicilombard.fire.model.jaxb.adapters.CustomTypes.unmarshalTimestamp(value));
    }

    public String marshal(Date value) {
        return (com.icicilombard.fire.model.jaxb.adapters.CustomTypes.marshalTimestamp(value));
    }

}
