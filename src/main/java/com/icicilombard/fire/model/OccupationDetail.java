//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2023.03.06 at 02:31:24 PM IST 
//


package com.icicilombard.fire.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for OccupationDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OccupationDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="industry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tariffSection" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="occupancy" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tariffOccupancy" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="riskCode" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="hazardGrade" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="occupancyType" type="{http://fire.icicilombard.com}OccupancyType"/>
 *         &lt;element name="EQDiscountFromTariffRate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="FLEXADiscountFromTariffRate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="fireDiscountFromTariffRate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="fireDiscountFromBaseRate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="occupancySegment" type="{http://fire.icicilombard.com}OccupancySegment"/>
 *         &lt;element name="terrorismRate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="EQSumInsured" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="EQPremium" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="firePremium" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="fireSumInsured" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="terrorismPremium" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="terrorismSumInsured" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="rateCode" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="warrantyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="warrantyDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="technicalRiskRate" type="{http://fire.icicilombard.com}TechnicalRiskRate" maxOccurs="3" minOccurs="0"/>
 *         &lt;element name="netRate" type="{http://fire.icicilombard.com}TechnicalRiskRate" maxOccurs="3" minOccurs="0"/>
 *         &lt;element name="coverRates" type="{http://fire.icicilombard.com}CoverRate" maxOccurs="2" minOccurs="0"/>
 *         &lt;element name="sectionThreeParameter" type="{http://fire.icicilombard.com}SectionThreeParameter" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OccupationDetail", propOrder = {
    "industry",
    "tariffSection",
    "occupancy",
    "tariffOccupancy",
    "riskCode",
    "hazardGrade",
    "occupancyType",
    "eqDiscountFromTariffRate",
    "flexaDiscountFromTariffRate",
    "fireDiscountFromTariffRate",
    "fireDiscountFromBaseRate",
    "occupancySegment",
    "terrorismRate",
    "eqSumInsured",
    "eqPremium",
    "firePremium",
    "fireSumInsured",
    "terrorismPremium",
    "terrorismSumInsured",
    "rateCode",
    "warrantyCode",
    "warrantyDesc",
    "technicalRiskRate",
    "netRate",
    "coverRates",
    "sectionThreeParameter"
})
public class OccupationDetail
    implements Serializable
{

    private final static long serialVersionUID = -6026937020915831338L;
    @XmlElement(required = true)
    protected String industry;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "integer")
    protected Integer tariffSection;
    @XmlElement(required = true)
    protected String occupancy;
    @XmlElement(required = true)
    protected String tariffOccupancy;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "integer")
    protected Integer riskCode;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "integer")
    protected Integer hazardGrade;
    @XmlElement(required = true)
    protected OccupancyType occupancyType;
    @XmlElement(name = "EQDiscountFromTariffRate", required = true)
    protected BigDecimal eqDiscountFromTariffRate;
    @XmlElement(name = "FLEXADiscountFromTariffRate", required = true)
    protected BigDecimal flexaDiscountFromTariffRate;
    @XmlElement(required = true)
    protected BigDecimal fireDiscountFromTariffRate;
    @XmlElement(required = true)
    protected BigDecimal fireDiscountFromBaseRate;
    @XmlElement(required = true)
    protected OccupancySegment occupancySegment;
    @XmlElement(required = true)
    protected BigDecimal terrorismRate;
    @XmlElement(name = "EQSumInsured", required = true)
    protected BigDecimal eqSumInsured;
    @XmlElement(name = "EQPremium", required = true)
    protected BigDecimal eqPremium;
    @XmlElement(required = true)
    protected BigDecimal firePremium;
    @XmlElement(required = true)
    protected BigDecimal fireSumInsured;
    @XmlElement(required = true)
    protected BigDecimal terrorismPremium;
    @XmlElement(required = true)
    protected BigDecimal terrorismSumInsured;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "integer")
    protected Integer rateCode;
    @XmlElement(required = true)
    protected String warrantyCode;
    @XmlElement(required = true)
    protected String warrantyDesc;
    protected com.icicilombard.fire.model.TechnicalRiskRate[] technicalRiskRate;
    protected com.icicilombard.fire.model.TechnicalRiskRate[] netRate;
    protected com.icicilombard.fire.model.CoverRate[] coverRates;
    protected com.icicilombard.fire.model.SectionThreeParameter[] sectionThreeParameter;

    /**
     * Gets the value of the industry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndustry() {
        return industry;
    }

    /**
     * Sets the value of the industry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndustry(String value) {
        this.industry = value;
    }

    /**
     * Gets the value of the tariffSection property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Integer getTariffSection() {
        return tariffSection;
    }

    /**
     * Sets the value of the tariffSection property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffSection(Integer value) {
        this.tariffSection = value;
    }

    /**
     * Gets the value of the occupancy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccupancy() {
        return occupancy;
    }

    /**
     * Sets the value of the occupancy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccupancy(String value) {
        this.occupancy = value;
    }

    /**
     * Gets the value of the tariffOccupancy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffOccupancy() {
        return tariffOccupancy;
    }

    /**
     * Sets the value of the tariffOccupancy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffOccupancy(String value) {
        this.tariffOccupancy = value;
    }

    /**
     * Gets the value of the riskCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Integer getRiskCode() {
        return riskCode;
    }

    /**
     * Sets the value of the riskCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskCode(Integer value) {
        this.riskCode = value;
    }

    /**
     * Gets the value of the hazardGrade property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Integer getHazardGrade() {
        return hazardGrade;
    }

    /**
     * Sets the value of the hazardGrade property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHazardGrade(Integer value) {
        this.hazardGrade = value;
    }

    /**
     * Gets the value of the occupancyType property.
     * 
     * @return
     *     possible object is
     *     {@link OccupancyType }
     *     
     */
    public OccupancyType getOccupancyType() {
        return occupancyType;
    }

    /**
     * Sets the value of the occupancyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link OccupancyType }
     *     
     */
    public void setOccupancyType(OccupancyType value) {
        this.occupancyType = value;
    }

    /**
     * Gets the value of the eqDiscountFromTariffRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEQDiscountFromTariffRate() {
        return eqDiscountFromTariffRate;
    }

    /**
     * Sets the value of the eqDiscountFromTariffRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEQDiscountFromTariffRate(BigDecimal value) {
        this.eqDiscountFromTariffRate = value;
    }

    /**
     * Gets the value of the flexaDiscountFromTariffRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFLEXADiscountFromTariffRate() {
        return flexaDiscountFromTariffRate;
    }

    /**
     * Sets the value of the flexaDiscountFromTariffRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFLEXADiscountFromTariffRate(BigDecimal value) {
        this.flexaDiscountFromTariffRate = value;
    }

    /**
     * Gets the value of the fireDiscountFromTariffRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFireDiscountFromTariffRate() {
        return fireDiscountFromTariffRate;
    }

    /**
     * Sets the value of the fireDiscountFromTariffRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFireDiscountFromTariffRate(BigDecimal value) {
        this.fireDiscountFromTariffRate = value;
    }

    /**
     * Gets the value of the fireDiscountFromBaseRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFireDiscountFromBaseRate() {
        return fireDiscountFromBaseRate;
    }

    /**
     * Sets the value of the fireDiscountFromBaseRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFireDiscountFromBaseRate(BigDecimal value) {
        this.fireDiscountFromBaseRate = value;
    }

    /**
     * Gets the value of the occupancySegment property.
     * 
     * @return
     *     possible object is
     *     {@link OccupancySegment }
     *     
     */
    public OccupancySegment getOccupancySegment() {
        return occupancySegment;
    }

    /**
     * Sets the value of the occupancySegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link OccupancySegment }
     *     
     */
    public void setOccupancySegment(OccupancySegment value) {
        this.occupancySegment = value;
    }

    /**
     * Gets the value of the terrorismRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTerrorismRate() {
        return terrorismRate;
    }

    /**
     * Sets the value of the terrorismRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTerrorismRate(BigDecimal value) {
        this.terrorismRate = value;
    }

    /**
     * Gets the value of the eqSumInsured property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEQSumInsured() {
        return eqSumInsured;
    }

    /**
     * Sets the value of the eqSumInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEQSumInsured(BigDecimal value) {
        this.eqSumInsured = value;
    }

    /**
     * Gets the value of the eqPremium property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEQPremium() {
        return eqPremium;
    }

    /**
     * Sets the value of the eqPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEQPremium(BigDecimal value) {
        this.eqPremium = value;
    }

    /**
     * Gets the value of the firePremium property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFirePremium() {
        return firePremium;
    }

    /**
     * Sets the value of the firePremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFirePremium(BigDecimal value) {
        this.firePremium = value;
    }

    /**
     * Gets the value of the fireSumInsured property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFireSumInsured() {
        return fireSumInsured;
    }

    /**
     * Sets the value of the fireSumInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFireSumInsured(BigDecimal value) {
        this.fireSumInsured = value;
    }

    /**
     * Gets the value of the terrorismPremium property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTerrorismPremium() {
        return terrorismPremium;
    }

    /**
     * Sets the value of the terrorismPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTerrorismPremium(BigDecimal value) {
        this.terrorismPremium = value;
    }

    /**
     * Gets the value of the terrorismSumInsured property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTerrorismSumInsured() {
        return terrorismSumInsured;
    }

    /**
     * Sets the value of the terrorismSumInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTerrorismSumInsured(BigDecimal value) {
        this.terrorismSumInsured = value;
    }

    /**
     * Gets the value of the rateCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Integer getRateCode() {
        return rateCode;
    }

    /**
     * Sets the value of the rateCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateCode(Integer value) {
        this.rateCode = value;
    }

    /**
     * Gets the value of the warrantyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWarrantyCode() {
        return warrantyCode;
    }

    /**
     * Sets the value of the warrantyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWarrantyCode(String value) {
        this.warrantyCode = value;
    }

    /**
     * Gets the value of the warrantyDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWarrantyDesc() {
        return warrantyDesc;
    }

    /**
     * Sets the value of the warrantyDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWarrantyDesc(String value) {
        this.warrantyDesc = value;
    }

    /**
     * 
     * 
     * @return
     *     array of
     *     {@link com.icicilombard.fire.model.TechnicalRiskRate }
     *     
     */
    public com.icicilombard.fire.model.TechnicalRiskRate[] getTechnicalRiskRate() {
        if (this.technicalRiskRate == null) {
            return new com.icicilombard.fire.model.TechnicalRiskRate[ 0 ] ;
        }
        com.icicilombard.fire.model.TechnicalRiskRate[] retVal = new com.icicilombard.fire.model.TechnicalRiskRate[this.technicalRiskRate.length] ;
        System.arraycopy(this.technicalRiskRate, 0, retVal, 0, this.technicalRiskRate.length);
        return (retVal);
    }

    /**
     * 
     * 
     * @return
     *     one of
     *     {@link com.icicilombard.fire.model.TechnicalRiskRate }
     *     
     */
    public com.icicilombard.fire.model.TechnicalRiskRate getTechnicalRiskRate(int idx) {
        if (this.technicalRiskRate == null) {
            throw new IndexOutOfBoundsException();
        }
        return this.technicalRiskRate[idx];
    }

    public int getTechnicalRiskRateLength() {
        if (this.technicalRiskRate == null) {
            return  0;
        }
        return this.technicalRiskRate.length;
    }

    /**
     * 
     * 
     * @param values
     *     allowed objects are
     *     {@link com.icicilombard.fire.model.TechnicalRiskRate }
     *     
     */
    public void setTechnicalRiskRate(com.icicilombard.fire.model.TechnicalRiskRate[] values) {
        int len = values.length;
        this.technicalRiskRate = ((com.icicilombard.fire.model.TechnicalRiskRate[]) new com.icicilombard.fire.model.TechnicalRiskRate[len] );
        for (int i = 0; (i<len); i ++) {
            this.technicalRiskRate[i] = values[i];
        }
    }

    /**
     * 
     * 
     * @param value
     *     allowed object is
     *     {@link com.icicilombard.fire.model.TechnicalRiskRate }
     *     
     */
    public com.icicilombard.fire.model.TechnicalRiskRate setTechnicalRiskRate(int idx, com.icicilombard.fire.model.TechnicalRiskRate value) {
        return this.technicalRiskRate[idx] = value;
    }

    /**
     * 
     * 
     * @return
     *     array of
     *     {@link com.icicilombard.fire.model.TechnicalRiskRate }
     *     
     */
    public com.icicilombard.fire.model.TechnicalRiskRate[] getNetRate() {
        if (this.netRate == null) {
            return new com.icicilombard.fire.model.TechnicalRiskRate[ 0 ] ;
        }
        com.icicilombard.fire.model.TechnicalRiskRate[] retVal = new com.icicilombard.fire.model.TechnicalRiskRate[this.netRate.length] ;
        System.arraycopy(this.netRate, 0, retVal, 0, this.netRate.length);
        return (retVal);
    }

    /**
     * 
     * 
     * @return
     *     one of
     *     {@link com.icicilombard.fire.model.TechnicalRiskRate }
     *     
     */
    public com.icicilombard.fire.model.TechnicalRiskRate getNetRate(int idx) {
        if (this.netRate == null) {
            throw new IndexOutOfBoundsException();
        }
        return this.netRate[idx];
    }

    public int getNetRateLength() {
        if (this.netRate == null) {
            return  0;
        }
        return this.netRate.length;
    }

    /**
     * 
     * 
     * @param values
     *     allowed objects are
     *     {@link com.icicilombard.fire.model.TechnicalRiskRate }
     *     
     */
    public void setNetRate(com.icicilombard.fire.model.TechnicalRiskRate[] values) {
        int len = values.length;
        this.netRate = ((com.icicilombard.fire.model.TechnicalRiskRate[]) new com.icicilombard.fire.model.TechnicalRiskRate[len] );
        for (int i = 0; (i<len); i ++) {
            this.netRate[i] = values[i];
        }
    }

    /**
     * 
     * 
     * @param value
     *     allowed object is
     *     {@link com.icicilombard.fire.model.TechnicalRiskRate }
     *     
     */
    public com.icicilombard.fire.model.TechnicalRiskRate setNetRate(int idx, com.icicilombard.fire.model.TechnicalRiskRate value) {
        return this.netRate[idx] = value;
    }

    /**
     * 
     * 
     * @return
     *     array of
     *     {@link com.icicilombard.fire.model.CoverRate }
     *     
     */
    public com.icicilombard.fire.model.CoverRate[] getCoverRates() {
        if (this.coverRates == null) {
            return new com.icicilombard.fire.model.CoverRate[ 0 ] ;
        }
        com.icicilombard.fire.model.CoverRate[] retVal = new com.icicilombard.fire.model.CoverRate[this.coverRates.length] ;
        System.arraycopy(this.coverRates, 0, retVal, 0, this.coverRates.length);
        return (retVal);
    }

    /**
     * 
     * 
     * @return
     *     one of
     *     {@link com.icicilombard.fire.model.CoverRate }
     *     
     */
    public com.icicilombard.fire.model.CoverRate getCoverRates(int idx) {
        if (this.coverRates == null) {
            throw new IndexOutOfBoundsException();
        }
        return this.coverRates[idx];
    }

    public int getCoverRatesLength() {
        if (this.coverRates == null) {
            return  0;
        }
        return this.coverRates.length;
    }

    /**
     * 
     * 
     * @param values
     *     allowed objects are
     *     {@link com.icicilombard.fire.model.CoverRate }
     *     
     */
    public void setCoverRates(com.icicilombard.fire.model.CoverRate[] values) {
        int len = values.length;
        this.coverRates = ((com.icicilombard.fire.model.CoverRate[]) new com.icicilombard.fire.model.CoverRate[len] );
        for (int i = 0; (i<len); i ++) {
            this.coverRates[i] = values[i];
        }
    }

    /**
     * 
     * 
     * @param value
     *     allowed object is
     *     {@link com.icicilombard.fire.model.CoverRate }
     *     
     */
    public com.icicilombard.fire.model.CoverRate setCoverRates(int idx, com.icicilombard.fire.model.CoverRate value) {
        return this.coverRates[idx] = value;
    }

    /**
     * 
     * 
     * @return
     *     array of
     *     {@link com.icicilombard.fire.model.SectionThreeParameter }
     *     
     */
    public com.icicilombard.fire.model.SectionThreeParameter[] getSectionThreeParameter() {
        if (this.sectionThreeParameter == null) {
            return new com.icicilombard.fire.model.SectionThreeParameter[ 0 ] ;
        }
        com.icicilombard.fire.model.SectionThreeParameter[] retVal = new com.icicilombard.fire.model.SectionThreeParameter[this.sectionThreeParameter.length] ;
        System.arraycopy(this.sectionThreeParameter, 0, retVal, 0, this.sectionThreeParameter.length);
        return (retVal);
    }

    /**
     * 
     * 
     * @return
     *     one of
     *     {@link com.icicilombard.fire.model.SectionThreeParameter }
     *     
     */
    public com.icicilombard.fire.model.SectionThreeParameter getSectionThreeParameter(int idx) {
        if (this.sectionThreeParameter == null) {
            throw new IndexOutOfBoundsException();
        }
        return this.sectionThreeParameter[idx];
    }

    public int getSectionThreeParameterLength() {
        if (this.sectionThreeParameter == null) {
            return  0;
        }
        return this.sectionThreeParameter.length;
    }

    /**
     * 
     * 
     * @param values
     *     allowed objects are
     *     {@link com.icicilombard.fire.model.SectionThreeParameter }
     *     
     */
    public void setSectionThreeParameter(com.icicilombard.fire.model.SectionThreeParameter[] values) {
        int len = values.length;
        this.sectionThreeParameter = ((com.icicilombard.fire.model.SectionThreeParameter[]) new com.icicilombard.fire.model.SectionThreeParameter[len] );
        for (int i = 0; (i<len); i ++) {
            this.sectionThreeParameter[i] = values[i];
        }
    }

    /**
     * 
     * 
     * @param value
     *     allowed object is
     *     {@link com.icicilombard.fire.model.SectionThreeParameter }
     *     
     */
    public com.icicilombard.fire.model.SectionThreeParameter setSectionThreeParameter(int idx, com.icicilombard.fire.model.SectionThreeParameter value) {
        return this.sectionThreeParameter[idx] = value;
    }

}
