//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2023.03.06 at 02:31:24 PM IST 
//


package com.icicilombard.fire.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SourceSystem.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SourceSystem">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ECHANNEL"/>
 *     &lt;enumeration value="PATHFINDER"/>
 *     &lt;enumeration value="ONLINEMANAGER"/>
 *     &lt;enumeration value="IAGENT"/>
 *     &lt;enumeration value="ASPIRE"/>
 *     &lt;enumeration value="IPARTNER"/>
 *     &lt;enumeration value="QMS"/>
 *     &lt;enumeration value="INDIABULLS"/>
 *     &lt;enumeration value="MOBILERTS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SourceSystem")
@XmlEnum
public enum SourceSystem {

    ECHANNEL,
    PATHFINDER,
    ONLINEMANAGER,
    IAGENT,
    ASPIRE,
    IPARTNER,
    QMS,
    INDIABULLS,
    MOBILERTS;

    public String value() {
        return name();
    }

    public static SourceSystem fromValue(String v) {
        return valueOf(v);
    }

}
