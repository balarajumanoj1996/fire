//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2023.03.06 at 02:31:24 PM IST 
//


package com.icicilombard.fire.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddonCoverType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AddonCoverType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="POLICY_LEVEL"/>
 *     &lt;enumeration value="LOCATION_LEVEL"/>
 *     &lt;enumeration value="BLOCK_LEVEL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AddonCoverType")
@XmlEnum
public enum AddonCoverType {

    POLICY_LEVEL,
    LOCATION_LEVEL,
    BLOCK_LEVEL;

    public String value() {
        return name();
    }

    public static AddonCoverType fromValue(String v) {
        return valueOf(v);
    }

}
