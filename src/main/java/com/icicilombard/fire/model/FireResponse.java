//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2023.03.06 at 02:31:24 PM IST 
//


package com.icicilombard.fire.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for FireResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FireResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sectionThreeValidDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="canFinalize" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="uwLevel" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="policyStatus" type="{http://fire.icicilombard.com}PolicyStatus"/>
 *         &lt;element name="messages" type="{http://fire.icicilombard.com}Messages"/>
 *         &lt;element name="taxDetails" type="{http://fire.icicilombard.com}TaxDetails"/>
 *         &lt;element name="policyRate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="basicPremium" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="totalPolicyBmaSI" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="riskInspectionRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="locationDetails" type="{http://fire.icicilombard.com}LocationDetail" maxOccurs="unbounded"/>
 *         &lt;element name="totalPremium" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="totalPremiumBeforeAddOnCover" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="totalSIBeforeAddOnCover" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="coInsuranceValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="floaterDetails" type="{http://fire.icicilombard.com}FloaterDetails"/>
 *         &lt;element name="shortPeriodDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="longPeriodDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="stampDutyDetails" type="{http://fire.icicilombard.com}StampDuty"/>
 *         &lt;element name="policyCover" type="{http://fire.icicilombard.com}PolicyCoverDetail" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="backdationDetails" type="{http://fire.icicilombard.com}BackdationDetails" minOccurs="0"/>
 *         &lt;element name="goalSeekAchieved" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="goalSeekResponse" type="{http://fire.icicilombard.com}GoalSeekOptions"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FireResponse", propOrder = {
    "requestID",
    "sectionThreeValidDate",
    "canFinalize",
    "uwLevel",
    "policyStatus",
    "messages",
    "taxDetails",
    "policyRate",
    "basicPremium",
    "totalPolicyBmaSI",
    "riskInspectionRequired",
    "locationDetails",
    "totalPremium",
    "totalPremiumBeforeAddOnCover",
    "totalSIBeforeAddOnCover",
    "coInsuranceValue",
    "floaterDetails",
    "shortPeriodDiscount",
    "longPeriodDiscount",
    "stampDutyDetails",
    "policyCover",
    "backdationDetails",
    "goalSeekAchieved",
    "goalSeekResponse"
})
public class FireResponse
    implements Serializable
{

    private final static long serialVersionUID = -6026937020915831338L;
    @XmlElement(required = true)
    protected String requestID;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "date")
    protected Date sectionThreeValidDate;
    protected boolean canFinalize;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "integer")
    protected Integer uwLevel;
    @XmlElement(required = true)
    protected PolicyStatus policyStatus;
    @XmlElement(required = true)
    protected Messages messages;
    @XmlElement(required = true)
    protected TaxDetails taxDetails;
    @XmlElement(required = true)
    protected BigDecimal policyRate;
    @XmlElement(required = true)
    protected BigDecimal basicPremium;
    @XmlElement(required = true)
    protected BigDecimal totalPolicyBmaSI;
    protected boolean riskInspectionRequired;
    @XmlElement(required = true)
    protected com.icicilombard.fire.model.LocationDetail[] locationDetails;
    @XmlElement(required = true)
    protected BigDecimal totalPremium;
    @XmlElement(required = true)
    protected BigDecimal totalPremiumBeforeAddOnCover;
    @XmlElement(required = true)
    protected BigDecimal totalSIBeforeAddOnCover;
    @XmlElement(required = true)
    protected BigDecimal coInsuranceValue;
    @XmlElement(required = true)
    protected FloaterDetails floaterDetails;
    @XmlElement(required = true)
    protected BigDecimal shortPeriodDiscount;
    @XmlElement(required = true)
    protected BigDecimal longPeriodDiscount;
    @XmlElement(required = true)
    protected StampDuty stampDutyDetails;
    protected com.icicilombard.fire.model.PolicyCoverDetail[] policyCover;
    protected BackdationDetails backdationDetails;
    protected boolean goalSeekAchieved;
    @XmlElement(required = true)
    protected GoalSeekOptions goalSeekResponse;

    /**
     * Gets the value of the requestID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestID() {
        return requestID;
    }

    /**
     * Sets the value of the requestID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestID(String value) {
        this.requestID = value;
    }

    /**
     * Gets the value of the sectionThreeValidDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getSectionThreeValidDate() {
        return sectionThreeValidDate;
    }

    /**
     * Sets the value of the sectionThreeValidDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSectionThreeValidDate(Date value) {
        this.sectionThreeValidDate = value;
    }

    /**
     * Gets the value of the canFinalize property.
     * 
     */
    public boolean isCanFinalize() {
        return canFinalize;
    }

    /**
     * Sets the value of the canFinalize property.
     * 
     */
    public void setCanFinalize(boolean value) {
        this.canFinalize = value;
    }

    /**
     * Gets the value of the uwLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Integer getUwLevel() {
        return uwLevel;
    }

    /**
     * Sets the value of the uwLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUwLevel(Integer value) {
        this.uwLevel = value;
    }

    /**
     * Gets the value of the policyStatus property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyStatus }
     *     
     */
    public PolicyStatus getPolicyStatus() {
        return policyStatus;
    }

    /**
     * Sets the value of the policyStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyStatus }
     *     
     */
    public void setPolicyStatus(PolicyStatus value) {
        this.policyStatus = value;
    }

    /**
     * Gets the value of the messages property.
     * 
     * @return
     *     possible object is
     *     {@link Messages }
     *     
     */
    public Messages getMessages() {
        return messages;
    }

    /**
     * Sets the value of the messages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Messages }
     *     
     */
    public void setMessages(Messages value) {
        this.messages = value;
    }

    /**
     * Gets the value of the taxDetails property.
     * 
     * @return
     *     possible object is
     *     {@link TaxDetails }
     *     
     */
    public TaxDetails getTaxDetails() {
        return taxDetails;
    }

    /**
     * Sets the value of the taxDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxDetails }
     *     
     */
    public void setTaxDetails(TaxDetails value) {
        this.taxDetails = value;
    }

    /**
     * Gets the value of the policyRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPolicyRate() {
        return policyRate;
    }

    /**
     * Sets the value of the policyRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPolicyRate(BigDecimal value) {
        this.policyRate = value;
    }

    /**
     * Gets the value of the basicPremium property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBasicPremium() {
        return basicPremium;
    }

    /**
     * Sets the value of the basicPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBasicPremium(BigDecimal value) {
        this.basicPremium = value;
    }

    /**
     * Gets the value of the totalPolicyBmaSI property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalPolicyBmaSI() {
        return totalPolicyBmaSI;
    }

    /**
     * Sets the value of the totalPolicyBmaSI property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalPolicyBmaSI(BigDecimal value) {
        this.totalPolicyBmaSI = value;
    }

    /**
     * Gets the value of the riskInspectionRequired property.
     * 
     */
    public boolean isRiskInspectionRequired() {
        return riskInspectionRequired;
    }

    /**
     * Sets the value of the riskInspectionRequired property.
     * 
     */
    public void setRiskInspectionRequired(boolean value) {
        this.riskInspectionRequired = value;
    }

    /**
     * 
     * 
     * @return
     *     array of
     *     {@link com.icicilombard.fire.model.LocationDetail }
     *     
     */
    public com.icicilombard.fire.model.LocationDetail[] getLocationDetails() {
        if (this.locationDetails == null) {
            return new com.icicilombard.fire.model.LocationDetail[ 0 ] ;
        }
        com.icicilombard.fire.model.LocationDetail[] retVal = new com.icicilombard.fire.model.LocationDetail[this.locationDetails.length] ;
        System.arraycopy(this.locationDetails, 0, retVal, 0, this.locationDetails.length);
        return (retVal);
    }

    /**
     * 
     * 
     * @return
     *     one of
     *     {@link com.icicilombard.fire.model.LocationDetail }
     *     
     */
    public com.icicilombard.fire.model.LocationDetail getLocationDetails(int idx) {
        if (this.locationDetails == null) {
            throw new IndexOutOfBoundsException();
        }
        return this.locationDetails[idx];
    }

    public int getLocationDetailsLength() {
        if (this.locationDetails == null) {
            return  0;
        }
        return this.locationDetails.length;
    }

    /**
     * 
     * 
     * @param values
     *     allowed objects are
     *     {@link com.icicilombard.fire.model.LocationDetail }
     *     
     */
    public void setLocationDetails(com.icicilombard.fire.model.LocationDetail[] values) {
        int len = values.length;
        this.locationDetails = ((com.icicilombard.fire.model.LocationDetail[]) new com.icicilombard.fire.model.LocationDetail[len] );
        for (int i = 0; (i<len); i ++) {
            this.locationDetails[i] = values[i];
        }
    }

    /**
     * 
     * 
     * @param value
     *     allowed object is
     *     {@link com.icicilombard.fire.model.LocationDetail }
     *     
     */
    public com.icicilombard.fire.model.LocationDetail setLocationDetails(int idx, com.icicilombard.fire.model.LocationDetail value) {
        return this.locationDetails[idx] = value;
    }

    /**
     * Gets the value of the totalPremium property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalPremium() {
        return totalPremium;
    }

    /**
     * Sets the value of the totalPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalPremium(BigDecimal value) {
        this.totalPremium = value;
    }

    /**
     * Gets the value of the totalPremiumBeforeAddOnCover property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalPremiumBeforeAddOnCover() {
        return totalPremiumBeforeAddOnCover;
    }

    /**
     * Sets the value of the totalPremiumBeforeAddOnCover property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalPremiumBeforeAddOnCover(BigDecimal value) {
        this.totalPremiumBeforeAddOnCover = value;
    }

    /**
     * Gets the value of the totalSIBeforeAddOnCover property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalSIBeforeAddOnCover() {
        return totalSIBeforeAddOnCover;
    }

    /**
     * Sets the value of the totalSIBeforeAddOnCover property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalSIBeforeAddOnCover(BigDecimal value) {
        this.totalSIBeforeAddOnCover = value;
    }

    /**
     * Gets the value of the coInsuranceValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoInsuranceValue() {
        return coInsuranceValue;
    }

    /**
     * Sets the value of the coInsuranceValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoInsuranceValue(BigDecimal value) {
        this.coInsuranceValue = value;
    }

    /**
     * Gets the value of the floaterDetails property.
     * 
     * @return
     *     possible object is
     *     {@link FloaterDetails }
     *     
     */
    public FloaterDetails getFloaterDetails() {
        return floaterDetails;
    }

    /**
     * Sets the value of the floaterDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link FloaterDetails }
     *     
     */
    public void setFloaterDetails(FloaterDetails value) {
        this.floaterDetails = value;
    }

    /**
     * Gets the value of the shortPeriodDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getShortPeriodDiscount() {
        return shortPeriodDiscount;
    }

    /**
     * Sets the value of the shortPeriodDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setShortPeriodDiscount(BigDecimal value) {
        this.shortPeriodDiscount = value;
    }

    /**
     * Gets the value of the longPeriodDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLongPeriodDiscount() {
        return longPeriodDiscount;
    }

    /**
     * Sets the value of the longPeriodDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLongPeriodDiscount(BigDecimal value) {
        this.longPeriodDiscount = value;
    }

    /**
     * Gets the value of the stampDutyDetails property.
     * 
     * @return
     *     possible object is
     *     {@link StampDuty }
     *     
     */
    public StampDuty getStampDutyDetails() {
        return stampDutyDetails;
    }

    /**
     * Sets the value of the stampDutyDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link StampDuty }
     *     
     */
    public void setStampDutyDetails(StampDuty value) {
        this.stampDutyDetails = value;
    }

    /**
     * 
     * 
     * @return
     *     array of
     *     {@link com.icicilombard.fire.model.PolicyCoverDetail }
     *     
     */
    public com.icicilombard.fire.model.PolicyCoverDetail[] getPolicyCover() {
        if (this.policyCover == null) {
            return new com.icicilombard.fire.model.PolicyCoverDetail[ 0 ] ;
        }
        com.icicilombard.fire.model.PolicyCoverDetail[] retVal = new com.icicilombard.fire.model.PolicyCoverDetail[this.policyCover.length] ;
        System.arraycopy(this.policyCover, 0, retVal, 0, this.policyCover.length);
        return (retVal);
    }

    /**
     * 
     * 
     * @return
     *     one of
     *     {@link com.icicilombard.fire.model.PolicyCoverDetail }
     *     
     */
    public com.icicilombard.fire.model.PolicyCoverDetail getPolicyCover(int idx) {
        if (this.policyCover == null) {
            throw new IndexOutOfBoundsException();
        }
        return this.policyCover[idx];
    }

    public int getPolicyCoverLength() {
        if (this.policyCover == null) {
            return  0;
        }
        return this.policyCover.length;
    }

    /**
     * 
     * 
     * @param values
     *     allowed objects are
     *     {@link com.icicilombard.fire.model.PolicyCoverDetail }
     *     
     */
    public void setPolicyCover(com.icicilombard.fire.model.PolicyCoverDetail[] values) {
        int len = values.length;
        this.policyCover = ((com.icicilombard.fire.model.PolicyCoverDetail[]) new com.icicilombard.fire.model.PolicyCoverDetail[len] );
        for (int i = 0; (i<len); i ++) {
            this.policyCover[i] = values[i];
        }
    }

    /**
     * 
     * 
     * @param value
     *     allowed object is
     *     {@link com.icicilombard.fire.model.PolicyCoverDetail }
     *     
     */
    public com.icicilombard.fire.model.PolicyCoverDetail setPolicyCover(int idx, com.icicilombard.fire.model.PolicyCoverDetail value) {
        return this.policyCover[idx] = value;
    }

    /**
     * Gets the value of the backdationDetails property.
     * 
     * @return
     *     possible object is
     *     {@link BackdationDetails }
     *     
     */
    public BackdationDetails getBackdationDetails() {
        return backdationDetails;
    }

    /**
     * Sets the value of the backdationDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link BackdationDetails }
     *     
     */
    public void setBackdationDetails(BackdationDetails value) {
        this.backdationDetails = value;
    }

    /**
     * Gets the value of the goalSeekAchieved property.
     * 
     */
    public boolean isGoalSeekAchieved() {
        return goalSeekAchieved;
    }

    /**
     * Sets the value of the goalSeekAchieved property.
     * 
     */
    public void setGoalSeekAchieved(boolean value) {
        this.goalSeekAchieved = value;
    }

    /**
     * Gets the value of the goalSeekResponse property.
     * 
     * @return
     *     possible object is
     *     {@link GoalSeekOptions }
     *     
     */
    public GoalSeekOptions getGoalSeekResponse() {
        return goalSeekResponse;
    }

    /**
     * Sets the value of the goalSeekResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link GoalSeekOptions }
     *     
     */
    public void setGoalSeekResponse(GoalSeekOptions value) {
        this.goalSeekResponse = value;
    }

}
