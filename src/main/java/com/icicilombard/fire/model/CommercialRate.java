//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2023.03.06 at 02:31:24 PM IST 
//


package com.icicilombard.fire.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommercialRate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommercialRate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="param" type="{http://fire.icicilombard.com}CommercialParam" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="commercialDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommercialRate", propOrder = {
    "param",
    "commercialDiscount"
})
public class CommercialRate
    implements Serializable
{

    private final static long serialVersionUID = -6026937020915831338L;
    protected com.icicilombard.fire.model.CommercialParam[] param;
    @XmlElement(required = true)
    protected BigDecimal commercialDiscount;

    /**
     * 
     * 
     * @return
     *     array of
     *     {@link com.icicilombard.fire.model.CommercialParam }
     *     
     */
    public com.icicilombard.fire.model.CommercialParam[] getParam() {
        if (this.param == null) {
            return new com.icicilombard.fire.model.CommercialParam[ 0 ] ;
        }
        com.icicilombard.fire.model.CommercialParam[] retVal = new com.icicilombard.fire.model.CommercialParam[this.param.length] ;
        System.arraycopy(this.param, 0, retVal, 0, this.param.length);
        return (retVal);
    }

    /**
     * 
     * 
     * @return
     *     one of
     *     {@link com.icicilombard.fire.model.CommercialParam }
     *     
     */
    public com.icicilombard.fire.model.CommercialParam getParam(int idx) {
        if (this.param == null) {
            throw new IndexOutOfBoundsException();
        }
        return this.param[idx];
    }

    public int getParamLength() {
        if (this.param == null) {
            return  0;
        }
        return this.param.length;
    }

    /**
     * 
     * 
     * @param values
     *     allowed objects are
     *     {@link com.icicilombard.fire.model.CommercialParam }
     *     
     */
    public void setParam(com.icicilombard.fire.model.CommercialParam[] values) {
        int len = values.length;
        this.param = ((com.icicilombard.fire.model.CommercialParam[]) new com.icicilombard.fire.model.CommercialParam[len] );
        for (int i = 0; (i<len); i ++) {
            this.param[i] = values[i];
        }
    }

    /**
     * 
     * 
     * @param value
     *     allowed object is
     *     {@link com.icicilombard.fire.model.CommercialParam }
     *     
     */
    public com.icicilombard.fire.model.CommercialParam setParam(int idx, com.icicilombard.fire.model.CommercialParam value) {
        return this.param[idx] = value;
    }

    /**
     * Gets the value of the commercialDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCommercialDiscount() {
        return commercialDiscount;
    }

    /**
     * Sets the value of the commercialDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCommercialDiscount(BigDecimal value) {
        this.commercialDiscount = value;
    }

}
