//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2023.03.06 at 02:31:24 PM IST 
//


package com.icicilombard.fire.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for ClaimExperience complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimExperience">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="coverType" type="{http://fire.icicilombard.com}CoverType"/>
 *         &lt;element name="claimYrs" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="claimPercentage" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="claimDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimExperience", propOrder = {
    "coverType",
    "claimYrs",
    "claimPercentage",
    "claimDiscount"
})
public class ClaimExperience
    implements Serializable
{

    private final static long serialVersionUID = -6026937020915831338L;
    @XmlElement(required = true)
    protected CoverType coverType;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "integer")
    protected Integer claimYrs;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "integer")
    protected Integer claimPercentage;
    @XmlElement(required = true)
    protected BigDecimal claimDiscount;

    /**
     * Gets the value of the coverType property.
     * 
     * @return
     *     possible object is
     *     {@link CoverType }
     *     
     */
    public CoverType getCoverType() {
        return coverType;
    }

    /**
     * Sets the value of the coverType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CoverType }
     *     
     */
    public void setCoverType(CoverType value) {
        this.coverType = value;
    }

    /**
     * Gets the value of the claimYrs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Integer getClaimYrs() {
        return claimYrs;
    }

    /**
     * Sets the value of the claimYrs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaimYrs(Integer value) {
        this.claimYrs = value;
    }

    /**
     * Gets the value of the claimPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Integer getClaimPercentage() {
        return claimPercentage;
    }

    /**
     * Sets the value of the claimPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaimPercentage(Integer value) {
        this.claimPercentage = value;
    }

    /**
     * Gets the value of the claimDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getClaimDiscount() {
        return claimDiscount;
    }

    /**
     * Sets the value of the claimDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setClaimDiscount(BigDecimal value) {
        this.claimDiscount = value;
    }

}
